// Design a data model using the same syntax we used in d1 for your upcoming capstone 2 project,
//which will be an ecommerce website.

// Specify at least the following collections: 

// - Users
// - Products


/*
Collection

  1) Users collection
  2) Product collection
  3) Orders Collection
  4) Campaign Collection


Document
  
  1) Users Specifics: 
- ID
	Data type: MongoDB ID
- username
	Data type: String
- password
	Data type: String
- email
	Data type: string
- user_info
	Array Objects
	Address - Data type: String
	Country - Data type: String
	Postal number - Data type: String
	Contact - Data type: String
- payment
	Array Objects
	Credit cards - Data type: String
	Paypal - Data type: String



  2 Product Specifics: 
- ID
	Data type: MongoDB ID
- name
	Data type: String
- description
	Data type: String
- price
	Data type: Integer
- category
	Data type: Array Objects (Each type is string)
- review
	Array Objects 
	user_ID - Data type: MongoDB ID
	Score - Data type: Integer
	Comments - Data type: String



  3) Orders Specifics: 
- ID
	Data type: MongoDB ID
- order_user
	Array Objects
	user_ID - Data type: MongoDB ID
- order_items
	Array Objects
	product_ID - Data type: MongoDB ID
	campaign_ID - Data type: MongoDB ID
	Quanity - Data type: Integer



   4) Campaign Specifies:
- ID 
	Data type: MongoDB ID
- Discount
	Array Objects
	product_ID - Data type: MongoDB ID
	order_ID - Data type: MongoDB ID
	user_ID - Data type: MongoDB ID
	new_price - Data type: Integer
	



